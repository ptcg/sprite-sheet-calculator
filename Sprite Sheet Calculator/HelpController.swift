//
//  HelpController.swift
//  Sprite Sheet Calculator
//
//  Created by Robert Keifer on 8/1/15.
//  Copyright (c) 2015 Tuxedo Cat Games. All rights reserved.
//

import Cocoa

class HelpController : NSViewController {
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear() {
        
        super.viewDidAppear()
        self.view.window?.styleMask = NSClosableWindowMask | NSTitledWindowMask | NSMiniaturizableWindowMask
    }
    
    override var representedObject: AnyObject? {
        
        didSet {
            // Update the view, if already loaded.
        }
    }
    
    @IBAction func doneButtonPressed(sender: NSButton) {
        
        dismissViewController(self)
    }
    
}
