//
//  SpriteSheetController.swift
//  Sprite Sheet Calculator
//
//  Created by Robert Keifer on 8/1/15.
//  Copyright (c) 2015 Tuxedo Cat Games. All rights reserved.
//

import Cocoa

class SpriteSheetController: NSViewController {

    @IBOutlet weak var imageWidthComboBox: NSComboBox!
    @IBOutlet weak var imageHeightComboBox: NSComboBox!
    @IBOutlet weak var sizeDivisorTextField: NSTextField!
    @IBOutlet var texturesTextView: NSTextView!
    @IBOutlet weak var statusLabel: NSTextField!
    @IBOutlet weak var copyTextureButton: NSButton!
    
    var output = ""
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        imageWidthComboBox.selectItemAtIndex(5)
        imageHeightComboBox.selectItemAtIndex(5)
    }
    
    override func viewDidAppear() {
        
        super.viewDidAppear()
        self.view.window?.styleMask = NSClosableWindowMask | NSTitledWindowMask | NSMiniaturizableWindowMask
    }
    
    override var representedObject: AnyObject? {
        
        didSet {
            // Update the view, if already loaded.
        }
    }
    
    @IBAction func processSpriteFileButtonPressed(sender: NSButton) {
        
        //get values
        let imageWidth = Int(imageWidthComboBox.stringValue)
        let imageHeight = Int(imageHeightComboBox.stringValue)
        let sizeDivisor = Float(sizeDivisorTextField.stringValue)
        
        //clear results
        texturesTextView.string = ""
        statusLabel.stringValue = ""
        
        //hide copy buttons
        copyTextureButton.hidden = true
        
        //validate values
        if sizeDivisor == nil {
            
            statusLabel.stringValue = "error: missing or invalid values"
            return
        }
        
        let filePicker = NSOpenPanel()
        
        filePicker.allowsMultipleSelection = false
        filePicker.canChooseFiles = true
        filePicker.canCreateDirectories = false
        
        filePicker.runModal()
        
        output = ""
        
        var spriteName = ""
        var left : Float = 0
        var top : Float = 0
        var right : Float = 0
        var bottom : Float = 0
        var height : Float = 0
        var width : Float = 0
        
        if let chosenFile = filePicker.URL {
            
            if let text = try? String(contentsOfURL: chosenFile, encoding: NSUTF8StringEncoding) {
                
                //separate values into rows
                let rows = text.componentsSeparatedByString("\n")
                
                //seperate rows and build texture values
                for (index, row) in rows.enumerate() {
                    
                    let values = row.componentsSeparatedByString(",")
                    
                    //sprite name
                    if values.count == 1 {
                        
                        spriteName = values[0].lowercaseString
                    }
                    else if values.count != 4 {
                        
                        statusLabel.stringValue = "error: incorrect number of values in row \(index)"
                        return
                    }
                    else {
                        
                        //dimensions
                        left = Float(Int(values[0])!) / Float(imageWidth!)
                        top = Float(Int(values[1])!) / Float(imageHeight!)
                        right = Float(Int(values[2])!) / Float(imageWidth!)
                        bottom = Float(Int(values[3])!) / Float(imageHeight!)
                        
                        width = (Float(Int(values[2])!) - Float(Int(values[0])!)) / Float(sizeDivisor!)
                        height = (Float(Int(values[3])!) - Float(Int(values[1])!)) / Float(sizeDivisor!)
                        
                        if right > 1.0 || bottom > 1.0 {
                            
                            statusLabel.stringValue = "error: sprite is larger than image"
                            return
                        }
                        
                        //create output
                        output += "\"\(spriteName)\" : {\n\n    \"size\" : { \"height\" : \(height), \"width\" : \(width) },\n"
                        output += "    \"texture\" : [\n\n        \(left), \(bottom),\n        \(right), \(bottom),\n        \(right), \(top),\n        \(left), \(top)\n    ]\n}"
                        
                        if index < rows.count - 1 {
                            
                            output += ",\n\n"
                        }
                    }
                }
                
                //output results
                texturesTextView.string = output
                
                statusLabel.stringValue = "calculating complete"
                
                //show copy buttons
                copyTextureButton.hidden = false
            }
        }
    }
    
    @IBAction func copyTextureButtonPressed(sender: NSButton) {
        
        NSPasteboard.generalPasteboard().clearContents()
        NSPasteboard.generalPasteboard().writeObjects([texturesTextView.string!])
        
        statusLabel.stringValue = "output copied to the clipboard"
    }
}
