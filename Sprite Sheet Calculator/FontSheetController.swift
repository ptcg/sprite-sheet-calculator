//
//  FontSheetController.swift
//  Sprite Sheet Calculator
//
//  Created by Robert Keifer on 8/1/15.
//  Copyright (c) 2015 Tuxedo Cat Games. All rights reserved.
//

import Cocoa

class FontSheetController: NSViewController {

    @IBOutlet weak var imageWidthComboBox: NSComboBox!
    @IBOutlet weak var imageHeightComboBox: NSComboBox!
    @IBOutlet var texturesTextView: NSTextView!
    @IBOutlet weak var statusLabel: NSTextField!
    @IBOutlet weak var copyTextureButton: NSButton!
    @IBOutlet weak var sizeDivisorTextField: NSTextField!
    
    var output = ""
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        imageWidthComboBox.selectItemAtIndex(5)
        imageHeightComboBox.selectItemAtIndex(5)
    }
    
    override func viewDidAppear() {
        
        super.viewDidAppear()
        self.view.window?.styleMask = NSClosableWindowMask | NSTitledWindowMask | NSMiniaturizableWindowMask
    }
    
    override var representedObject: AnyObject? {
        
        didSet {
            // Update the view, if already loaded.
        }
    }
    
    @IBAction func processFontFileButtonPressed(sender: NSButton) {
        
        //get values
        let imageWidth = Int(imageWidthComboBox.stringValue)
        let imageHeight = Int(imageHeightComboBox.stringValue)
        let sizeDivisor = Float(sizeDivisorTextField.stringValue)
        
        //clear results
        texturesTextView.string = ""
        statusLabel.stringValue = ""
        
        //hide copy buttons
        copyTextureButton.hidden = true
        
        //validate values
        if sizeDivisor == nil {
            
            statusLabel.stringValue = "error: missing or invalid values"
            return
        }
        
        let filePicker = NSOpenPanel()
        
        filePicker.allowsMultipleSelection = false
        filePicker.canChooseFiles = true
        filePicker.canCreateDirectories = false
        
        filePicker.runModal()
        
        var y : Float = 0;
        var x : Float = 0;
        
        var counter = 1
        var charHeight : Float = 0
        var charWidth : Float = 0
        var maxWidth = 0
        var maxHeight = 0
        var scale = [Int]()
        
        var scaleValues = ""
        var frameValues = "        0, 0,\n"
        
        if let chosenFile = filePicker.URL {
            
            if let text = try? String(contentsOfURL: chosenFile, encoding: NSUTF8StringEncoding) {
                
                //separate values into rows
                let rows = text.componentsSeparatedByString("\n")
                let lastRow = rows.count - 2
                
                //separate rows and build frames list
                for (rowNum, row) in rows.enumerate() {
                    
                    let values = row.componentsSeparatedByString(",")
                    let lastCol = values.count - 2
                    
                    //Y position rows
                    if values.count == 1 {
                        
                        y = Float(Int(values[0])!) / Float(imageHeight!)
                        
                        if y > 1 {
                            
                            statusLabel.stringValue = "error: font position outside of image height"
                            return
                        }
                        
                        //get the first height value for the texture #s later
                        if charHeight == 0 && y != 0 {
                            
                            charHeight = y
                        }
                    }
                    else {
                        
                        //X position rows
                        for (colNum, value) in values.enumerate() {
                            
                            x = Float(Int(value)!) / Float(imageWidth!)
                            
                            if x > 1 {
                                
                                statusLabel.stringValue = "error: font position outside of image width"
                                return
                            }
                            
                            if (x != 0 || y != 0) && (value != values.last) {
                                
                                if rowNum == lastRow && colNum == lastCol {
                                    
                                    frameValues += "        \(x), \(y)"
                                }
                                else {
                                    
                                    frameValues += "        \(x), \(y),\n"
                                }
                                
                                counter++
                            }
                        }
                    }
                }
                
                //build scale list
                for row in rows {
                    
                    let values = row.componentsSeparatedByString(",")
                    
                    if values.count == 1 {
                        
                        if maxHeight == 0 {
                            
                            //save height in pixels for later
                            maxHeight = Int(values[0])!
                        }
                    }
                    else {
                        
                        var previous = 0
                        
                        for value in values {
                            
                            let current = Int(value)!
                            
                            if current == 0 {
                                
                                //do nothing with 1st value
                            }
                            else {
                                
                                let currentWidth = current - previous
                                
                                scale.append(currentWidth)
                                
                                //get max width in pixels for later
                                if maxWidth < currentWidth {
                                    
                                    maxWidth = currentWidth
                                }
                                
                                previous = current
                            }
                        }
                    }
                }
                
                //now we have a list of widths and the max one, calculate the scale of each
                for (index, num) in scale.enumerate() {
                    
                    if index == scale.count - 1 {
                        
                        //last character, don't add linebreak
                        scaleValues += "        \(Float(num) / Float(maxWidth))"
                    }
                    else {
                        
                        scaleValues += "        \(Float(num) / Float(maxWidth)),\n"
                    }
                }
                
                charWidth = Float(maxWidth) / Float(imageWidth!)
                let height : Float = Float(maxHeight) / Float(sizeDivisor!)
                let width : Float = Float(maxWidth) / Float(sizeDivisor!)
                
                //output results
                output = "\"FONT_NAME\" : {\n\n    \"size\" : { \"height\" : \(height), \"width\" : \(width) },\n"
                
                //vertices
                output += "    \"vertices\" : [\n\n        0, 0,\n        \(width), 0,\n        \(width), \(height),\n        0, \(height)\n    ],\n"
                
                //texture
                output += "    \"texture\" : [\n\n        0, \(charHeight),\n        \(charWidth), \(charHeight),\n        \(charWidth), 0,\n        0, 0\n    ],\n"
                
                //frames
                output += "    \"frames\" : [\n\n" + frameValues + "\n    ],\n"
                
                //scale
                output += "    \"scale\" : [\n\n" + scaleValues + "\n    ]\n}"
                
                texturesTextView.string = output
                
                statusLabel.stringValue = "calculating complete"
                
                //show copy buttons
                copyTextureButton.hidden = false
            }
        }
    }
    
    @IBAction func copyTextureButtonPressed(sender: NSButton) {
        
        NSPasteboard.generalPasteboard().clearContents()
        NSPasteboard.generalPasteboard().writeObjects([texturesTextView.string!])
        
        statusLabel.stringValue = "output copied to the clipboard"
    }
}
