//
//  AnimationSheetController.swift
//  Sprite Sheet Calculator
//
//  Created by Robert Keifer on 8/1/15.
//  Copyright (c) 2015 Tuxedo Cat Games. All rights reserved.
//

import Cocoa

class AnimationSheetController : NSViewController {
    
    @IBOutlet weak var imageWidthComboBox: NSComboBox!
    @IBOutlet weak var imageHeightComboBox: NSComboBox!
    @IBOutlet weak var spriteWidthTextField: NSTextField!
    @IBOutlet weak var spriteHeightTextField: NSTextField!
    @IBOutlet weak var numberOfSpritesTextField: NSTextField!
    @IBOutlet var texturesTextView: NSTextView!
    @IBOutlet weak var statusLabel: NSTextField!
    @IBOutlet weak var copyTextureButton: NSButton!
    @IBOutlet weak var sizeDivisorTextField: NSTextField!
    
    var output = ""
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        imageWidthComboBox.selectItemAtIndex(5)
        imageHeightComboBox.selectItemAtIndex(5)
    }
    
    override func viewDidAppear() {
        
        super.viewDidAppear()
        self.view.window?.styleMask = NSClosableWindowMask | NSTitledWindowMask | NSMiniaturizableWindowMask
    }
    
    override var representedObject: AnyObject? {
        
        didSet {
            // Update the view, if already loaded.
        }
    }
    
    @IBAction func calculateButtonPressed(sender: NSButton) {
        
        //get values
        let imageWidth = Int(imageWidthComboBox.stringValue)
        let imageHeight = Int(imageHeightComboBox.stringValue)
        let spriteWidth = Int(spriteWidthTextField.stringValue)
        let spriteHeight = Int(spriteHeightTextField.stringValue)
        let numSprites = Int(numberOfSpritesTextField.stringValue)
        let sizeDivisor = Float(sizeDivisorTextField.stringValue)
        
        //clear results
        texturesTextView.string = ""
        statusLabel.stringValue = ""
        
        //hide copy buttons
        copyTextureButton.hidden = true
        
        //validate values
        if spriteWidth == nil || spriteHeight == nil || numSprites == nil || sizeDivisor == nil {
            
            statusLabel.stringValue = "error: missing or invalid values"
            return
        }
        
        if spriteHeight! > imageHeight! || spriteWidth! > imageWidth! {
            
            statusLabel.stringValue = "error: sprite is larger than image"
            return
        }
        
        //calculate sprite dimensions
        let width = Float(spriteWidth!) / Float(imageWidth!)
        let height = Float(spriteHeight!) / Float(imageHeight!)
        
        //compile output
        output = "\"SPRITE_NAME\" : {\n\n    \"size\" : { \"height\" : \(Float(spriteHeight!) / Float(sizeDivisor!)), \"width\" : \(Float(spriteWidth!) / Float(sizeDivisor!)) },\n    \"texture\" : [\n\n"
        output += "        0, \(height),\n        \(width), \(height),\n        \(width), 0,\n        0, 0\n    ]"
        
        if numSprites > 1 {
        
            output += ",\n    \"frames\" : [\n\n        0, 0"
            
            //calculate frame values
            var counter = 1
            var currentX : Float = 0
            var currentY : Float = 0
            
            while counter < numSprites! {
                
                currentX += width
                
                if currentX + width > 1.0 {
                    
                    currentX = 0
                    currentY += height
                    
                    if currentY + height > 1.0 {
                        
                        statusLabel.stringValue = "error: cannot make \(numSprites!) sprites of image size specified"
                        return
                    }
                }
                
                output += ",\n        \(currentX), \(currentY)"
                
                counter++
            }
            
            output += "\n    ]"
        }
        
        output += "\n}"
        
        //output results
        texturesTextView.string = output
        
        statusLabel.stringValue = "calculating complete"
        
        //show copy buttons
        copyTextureButton.hidden = false
    }
    
    @IBAction func copyTextureButtonPressed(sender: NSButton) {
        
        NSPasteboard.generalPasteboard().clearContents()
        NSPasteboard.generalPasteboard().writeObjects([texturesTextView.string!])
        
        statusLabel.stringValue = "output copied to the clipboard"
    }
}